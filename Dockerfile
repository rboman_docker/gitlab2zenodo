FROM python:3.6
# avoids interactive prompt for "tzdata"
ENV DEBIAN_FRONTEND=noninteractive

# ------------------------------------------------------------------------------
# libraries
# ------------------------------------------------------------------------------

RUN apt-get -y update && apt-get install -y --no-install-recommends \
    git-lfs \
  && rm -rf /var/lib/apt/lists/*

# install latest gitlab2zenodo

RUN cd /tmp \
  && git clone https://gitlab.com/sbeniamine/gitlab2zenodo.git \
  && cd gitlab2zenodo \
  && python setup.py install \
  && cd .. \
  && rm -rf gitlab2zenodo
